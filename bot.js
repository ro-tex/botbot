const Discord = require('discord.js')
const auth = require('./auth.json')

const client = new Discord.Client()

client.on('ready', () => { console.log(`Logged in as: ${client.user.tag}`) })

client.on('message', (msg) => {
  // don't edit own messages
  if (msg.author.id == client.user.id) return

  const matches = [...msg.content.matchAll(/sia:\/\/([a-zA-Z0-9_]+)/g)]
  const links = matches.map(([_, siaId]) => `https://siasky.net/${siaId}`)

  if (links.length > 0) {
    msg.channel.send(links.join("\n"))
  }
})

client.login(auth.token)
